var routes = [];
routes.clear = function() {
	while(this.length > 0) {
		var route = this.shift();
		route.remove();
	}
}

if(Function.prototype.bind === undefined) {
	Function.prototype.bind = function(thisArg) {
		var self = this;
		var savedArgs = argsToArray(arguments).splice(1);		
		
		return function() {
			self.apply(thisArg, savedArgs.concat(argsToArray(arguments)))
		};
	}
}

function argsToArray(args) {
	var arr = [];
	
	for(var i = args.length - 1; i >= 0; i--) {
		arr.unshift(args[i]);
	}
	
	return arr;
}

function GoogleMapRoute(map, origin, destination, color) {
	this.map = map;
	this.origin = origin;
	this.destination = destination;
	this.color = color;
	this.error = false;
	this.directionsService = new google.maps.DirectionsService();
	this.directionsDisplay = new google.maps.DirectionsRenderer();
};

GoogleMapRoute.prototype.getRoute = function() {
	var wayPoints = [];
	var args = argsToArray(arguments);
	
	for(var i = 0, len = args.length; i < len; i++) {
		args[i] != '' && wayPoints.push({location: args[i], stopover: false});		
	}
	
	this.directionsService.route({
		origin: this.origin,
		destination: this.destination,
		travelMode: google.maps.TravelMode.DRIVING,
		waypoints: wayPoints,
		optimizeWaypoints : true
	}, this.handleRouteResponse.bind(this));
};

GoogleMapRoute.prototype.handleRouteResponse = function(result, status) {
	if(status === google.maps.DirectionsStatus.OK) {
		this.bounds = result.routes[0].bounds;
		
		this.directionsDisplay.setOptions({
			polylineOptions: {
				strokeColor: this.color,
				strokeOpacity: 0.7
			},
			map: this.map
		});
		this.directionsDisplay.setDirections(result);
	}
};

GoogleMapRoute.prototype.remove = function() {
	this.directionsDisplay.setMap(null);
};

GoogleMapRoute.prototype.checkValues = function() {
	var values = [this.origin, this.destination];
	this.error = false;
	
	for(var i = 0, len = values.length; i < len; i++) {
		if(values[i] == '') {
			this.error = true;
		}
	}
};

GoogleMapRoute.prototype.render = function() {
	if(this.checkValues()) return false;		
	this.getRoute.apply(this, arguments);
};

function init() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 6,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	
	map.setCenter(new google.maps.LatLng(40.69847032728747, -73.9514422416687));
		
	var fitBounds = function(bounds1, bounds2) {
		var sw = {};
		var ne = {};
		sw.lat = bounds1.sw.lat() < bounds2.sw.lat() ? bounds1.sw.lat() : bounds2.sw.lat();
		sw.lon = bounds1.sw.lon() < bounds2.sw.lon() ? bounds1.sw.lon() : bounds2.sw.lon();
		ne.lat = bounds1.sw.lat() > bounds2.sw.lat() ? bounds1.sw.lat() : bounds2.sw.lat();
		sw.lon = bounds1.sw.lon() > bounds2.sw.lon() ? bounds1.sw.lon() : bounds2.sw.lon();
		map.fitBounds({sw: sw, ne: ne});
	};
	
	document.forms[0].onsubmit = function(e) {
		if(e.preventDefault){  
  			e.preventDefault();  
 		} else {  
  			e.returnValue = false;  
  			e.cancelBubble=true;  
 		}
 		
 		routes.clear();
 		
 		var origin1 = document.getElementById('address1').value;
		var destination1 = document.getElementById('address4').value;
		var color1 = document.getElementById('route1Color').value;
		var waypoint11 = document.getElementById('address2').value;
 		var waypoint12 = document.getElementById('address3').value;
		routes.push(new GoogleMapRoute(map, origin1, destination1, color1));
		
		var origin2 = document.getElementById('address5').value;
		var destination2 = document.getElementById('address8').value;
		var color2 = document.getElementById('route2Color').value;
		var waypoint21 = document.getElementById('address6').value;
 		var waypoint22 = document.getElementById('address7').value;
		routes.push(new GoogleMapRoute(map, origin2, destination2, color2));		
 		
 		routes[0].render(waypoint11, waypoint12);
 		routes[1].render(waypoint21, waypoint22);
	};
}

window.onload = init;